<?php

namespace UnderTheFlag\Lang\Resolvers;

use UnderTheFlag\Lang\Translator;

interface ITranslateResolver
{

	/**
	 * @param Translator $translator
	 *
	 * @return string|null
	 */
	public function resolve(Translator $translator): ?string;

}
