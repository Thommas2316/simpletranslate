<?php

namespace UnderTheFlag\Lang\Resolvers;

use Nette\Application\Application;
use Nette\Application\Request;
use Nette\SmartObject;
use UnderTheFlag\Lang\Translator;

class LocaleParamResolver implements ITranslateResolver
{
	use SmartObject;

	/**
	 * @var Request
	 */
	private $request;

	/**
	 * @var Translator
	 */
	private $translator;

	public function setTranslator(Translator $translator)
	{
		$this->translator = $translator;
	}

	/**
	 * @param Application $application
	 * @param Request     $request
	 *
	 * @return string|null
	 */
	public function onRequest(Application $application, Request $request): ?string
	{
		$params = $request->getParameters();
		if ($request->getMethod() === Request::FORWARD && empty($params['locale'])) {
			return NULL;
		}

		$this->request = $request;

		if ( ! $this->translator) {
			return NULL;
		}

		$this->translator->setLocale(NULL);

		return $this->translator->getLocale(); // invoke resolver
	}

	/**
	 * @param Translator $translator
	 *
	 * @return string|null
	 */
	public function resolve(Translator $translator): ?string
	{
		if ($this->request === NULL) {
			return NULL;
		}

		$params = $this->request->getParameters();

		return FALSE === empty($params['locale']) ? $params['locale'] : NULL;
	}

}
