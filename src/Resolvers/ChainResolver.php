<?php

/**
 * This file is part of the Kdyby (http://www.kdyby.org)
 *
 * Copyright (c) 2008 Filip Procházka (filip@prochazka.su)
 *
 * For the full copyright and license information, please view the file license.txt that was distributed with this
 * source code.
 */

namespace UnderTheFlag\Lang\Resolvers;

use UnderTheFlag\Lang\Translator;

class ChainResolver implements ITranslateResolver
{

	/**
	 * @var array|ITranslateResolver[]
	 */
	private $resolvers = [];

	/**
	 * @param ITranslateResolver $resolver
	 */
	public function addResolver(ITranslateResolver $resolver)
	{
		array_unshift($this->resolvers, $resolver); // first the newer
	}

	/**
	 * @param Translator $translator
	 *
	 * @return string|null
	 */
	public function resolve(Translator $translator): ?string
	{
		foreach ($this->resolvers as $resolver) {
			$locale = $resolver->resolve($translator);
			if ($locale !== NULL) {
				return $locale;
			}
		}

		return $translator->getDefaultLocale();
	}

}
