<?php

namespace UnderTheFlag\Lang;

use Nette\Localization\ITranslator;
use Nette\Utils\Strings;
use Symfony\Component\Translation\Exception\InvalidArgumentException;
use UnderTheFlag\Lang\Resolvers\ITranslateResolver;

class Translator extends \Symfony\Component\Translation\Translator implements ITranslator
{

	/** @var ITranslateResolver */
	private $resolver;

	/** @var null|string */
	private $defaultLocale = NULL;

	/**
	 * Translator constructor.
	 *
	 * @param string $locale
	 *
	 * @throws InvalidArgumentException
	 */
	public function __construct(ITranslateResolver $resolver, string $cacheDirectory)
	{
		$this->resolver = $resolver;
		parent::__construct(NULL, NULL, $cacheDirectory);
		$this->setLocale(NULL);
	}

	/**
	 * Returns the current locale.
	 *
	 * @return string|null
	 */
	public function getLocale(): ?string
	{
		if (parent::getLocale() === NULL) {
			$this->setLocale($this->resolver->resolve($this));
		}

		return parent::getLocale();
	}

	/**
	 * @param string $message
	 * @param mixed  ...$parameters
	 *
	 * @return string
	 * @throws InvalidArgumentException
	 */
	public function translate($message, ...$parameters): string
	{
		$params = $parameters[0] ?? [];

		if (count($params)) {
			$params = $this->cleanupParameters($params);
		}

		return $this->trans($message, $params);
	}

	/**
	 * @param string $message
	 * @param array  $parameters
	 * @param null   $domain
	 * @param null   $locale
	 *
	 * @return string
	 */
	public function trans($message, array $parameters = [], $domain = NULL, $locale = NULL): string
	{
		if ($domain === NULL) {
			list($domain, $id) = $this->extractMessageDomain($message);
		} else {
			$id = $message;
		}

		$result = parent::trans($id, $parameters, $domain, $locale);

		return $result;
	}

	/**
	 * @param array $parameters
	 *
	 * @return <string,string>[]
	 */
	private function cleanupParameters(array $parameters): array
	{
		$cleanParameters = [];

		/**
		 * @var string $parameterKey
		 * @var string $parameterValue
		 */
		foreach ($parameters as $parameterKey => $parameterValue) {
			$parameterKey                   = sprintf('%s%s%s',
				FALSE === Strings::startsWith($parameterKey, '%') ? '%' : '',
				$parameterKey,
				FALSE === Strings::endsWith($parameterKey, '%') ? '%' : ''
			);
			$cleanParameters[$parameterKey] = $parameterValue;
		}

		return $cleanParameters;
	}

	/**
	 * @param string $message
	 *
	 * @return array
	 */
	private function extractMessageDomain($message)
	{
		if (strpos($message, '.') !== FALSE && strpos($message, ' ') === FALSE) {
			list($domain, $message) = explode('.', $message, 2);
		} else {
			$domain = 'messages';
		}

		return [$domain, $message];
	}

	/**
	 * @return string|null
	 */
	public function getDefaultLocale(): ?string
	{
		return $this->defaultLocale;
	}

	/**
	 * @param string $locale
	 *
	 * @return Translator
	 */
	public function setDefaultLocale(string $locale): Translator
	{
		$this->assertValidLocale($locale);
		$this->defaultLocale = $locale;

		return $this;
	}
}