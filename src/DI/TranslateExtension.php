<?php

/**
 * This file is part of the Kdyby (http://www.kdyby.org)
 *
 * Copyright (c) 2008 Filip Procházka (filip@prochazka.su)
 *
 * For the full copyright and license information, please view the file license.txt that was distributed with this
 * source code.
 */

namespace UnderTheFlag\Lang\DI;

use Nette\Application\Application;
use Nette\DI\Definitions\Definition;
use Nette\Utils\Callback;
use Nette\Utils\Finder;
use Nette\Utils\Validators;
use Symfony\Component\Translation\Exception\InvalidResourceException;
use Symfony\Component\Translation\Loader\LoaderInterface;
use Tracy\IBarPanel;
use UnderTheFlag\Lang\Resolvers\ChainResolver;
use UnderTheFlag\Lang\Resolvers\LocaleParamResolver;
use UnderTheFlag\Lang\Translator;

class TranslateExtension extends \Nette\DI\CompilerExtension
{
	const TAG_LOADER = 'translate.loader';

	/** @var array */
	private $loaders = [];

	/** @var bool */
	private $debugMode;

	/** @var string|null */
	private $tempDir;

	/**
	 * @var mixed[]
	 */
	private $defaults = [
		'dirs'     => ['%appDir%/lang', '%appDir%/locale'],
		'default'  => 'en',
		'fallback' => [],
		'debugger' => '%debugMode%',
		'cacheDir' => '%tempDir%/cache',
	];

	/**
	 * @throws \Nette\Utils\AssertionException
	 */
	public function loadConfiguration()
	{
		$builder = $this->getContainerBuilder();
		$config  = $this->validateConfig($this->defaults);

		$this->loadLoaders();
		foreach ($builder->findByTag(self::TAG_LOADER) as $loaderId => $meta) {
			Validators::assert($meta, 'string:2..');
			$builder->getDefinition($loaderId)->setAutowired(FALSE);
			$this->loaders[$meta] = $loaderId;
		}

		$builder->addDefinition($this->prefix('default'))
		        ->setFactory(Translator::class,
			        [$this->prefix('@userLocaleResolver'), $config['cacheDir']])
		        ->addSetup('?->setTranslator(?)', [$this->prefix('@userLocaleResolver.param'), '@self'])
		        ->addSetup('setDefaultLocale', [$config['default']]);

		$dirs = array_values(array_filter($config['dirs'], Callback::closure('is_dir')));
		if (count($dirs) > 0) {
			$this->loadResourcesFromDirs($dirs);
		}

		$this->loadLocaleResolver($config);
	}

	protected function loadLoaders(): void
	{
		$loaders = $this->loadFromFile(__DIR__ . '/config/loaders.neon');
		$builder = $this->getContainerBuilder();

		foreach ($loaders as $format => $class) {
			$builder->addDefinition($this->prefix('loader.' . $format))
			        ->setClass($class)
			        ->addTag(self::TAG_LOADER, $format);
		}
	}

	protected function loadLocaleResolver(array $config)
	{
		$builder = $this->getContainerBuilder();

		$builder->addDefinition($this->prefix('userLocaleResolver.param'))
		        ->setClass(LocaleParamResolver::class)
		        ->setAutowired(FALSE);

		$chain = $builder->addDefinition($this->prefix('userLocaleResolver'))
		                 ->setClass(ChainResolver::class);

		$resolvers = [];

		$resolvers[] = $this->prefix('@userLocaleResolver.param');
		$chain->addSetup('addResolver', [$this->prefix('@userLocaleResolver.param')]);

		if ($config['debugger'] && interface_exists(IBarPanel::class)) {
			$builder->getDefinition($this->prefix('panel'))
			        ->addSetup('setLocaleResolvers', [array_reverse($resolvers)]);
		}
	}

	protected function loadResourcesFromDirs(array $dirs)
	{
		$builder = $this->getContainerBuilder();

		$translator = $builder->getDefinition($this->prefix('default'));

		$translator->addSetup('addLoader', ['neon', $builder->getDefinition($this->loaders['neon'])]);

		foreach (Finder::findFiles(['*.neon'])->from($dirs) as $file) {

			/** @var \SplFileInfo $file */
			$fileName   = str_replace($dirs, '', $file->getPathname());
			$matchFound = preg_match('~^\/(?P<locale>[^\.]+)\/(?P<domain>.*?)\.(?P<format>[^\.]+)$~', $fileName, $m);
			if (FALSE === $matchFound) {
				continue;
			}

			$this->validateResource($file->getPathname(), $m['locale'], $m['domain'], $m['format']);
			$translator->addSetup('addResource', [$m['format'], $file->getPathname(), $m['locale'], $m['domain']]);
			$builder->addDependency($file->getPathname());
		}
	}

	/**
	 * @param string $format
	 * @param string $file
	 * @param string $locale
	 * @param string $domain
	 */
	protected function validateResource($file, $locale, $domain, $format = 'neon')
	{
		$builder = $this->getContainerBuilder();

		try {
			/** @var Definition $def */
			$def        = $builder->getDefinition($this->loaders[$format]);
			$reflection = new \ReflectionClass($def->getType());

			$loader = $reflection->newInstance();
			if ( ! $loader instanceof LoaderInterface) {
				return;
			}
		} catch (\ReflectionException $e) {
			return;
		}

		try {
			$loader->load($file, $locale, $domain);
		} catch (\Exception $e) {
			throw new InvalidResourceException(sprintf(
				'Resource %s is not valid and cannot be loaded.', $file
			), 0, $e);
		}
	}

	public function beforeCompile()
	{
		$builder = $this->getContainerBuilder();
		$config  = $this->getConfig();

		$applicationService = $builder->getByType(Application::class) ?: 'application';
		if ($builder->hasDefinition($applicationService)) {
			$builder->getDefinition($applicationService)
			        ->addSetup('$service->onRequest[] = ?',
				        [[$this->prefix('@userLocaleResolver.param'), 'onRequest']]);

			if ($config['debugger'] && interface_exists(IBarPanel::class)) {
				$builder->getDefinition($applicationService)
				        ->addSetup('$self = $this; $service->onStartup[] = function () use ($self) { $self->getService(?); }',
					        [$this->prefix('default')])
				        ->addSetup('$service->onRequest[] = ?', [[$this->prefix('@panel'), 'onRequest']]);
			}
		}
	}
}
